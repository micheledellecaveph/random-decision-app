var randomize = document.getElementById("randomize");
var listItems = document.getElementById("list").getElementsByTagName("li");
var result = document.getElementById("result");

randomize.addEventListener("click", randomizeIt);

function randomizeIt () {
    var randomItem = listItems[Math.floor(Math.random() * listItems.length)];
    result.innerHTML = randomItem.childNodes[0].value;
    console.log(randomItem);
}